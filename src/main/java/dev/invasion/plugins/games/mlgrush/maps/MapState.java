package dev.invasion.plugins.games.mlgrush.maps;

public enum MapState {
    WAITING,
    GAME,
    BUILD
}
